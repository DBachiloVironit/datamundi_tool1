import { DatamundiTool1Page } from './app.po';

describe('datamundi-tool1 App', () => {
  let page: DatamundiTool1Page;

  beforeEach(() => {
    page = new DatamundiTool1Page();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
