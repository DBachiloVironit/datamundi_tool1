import {Injectable} from '@angular/core';
import {Headers, Http, RequestMethod, RequestOptions, RequestOptionsArgs, Response} from '@angular/http';
import {Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';
import {IRequestResult} from '../../shared/RequestResult';
import {ErrorCodeProcessor, IErrorInfo} from '../error-code-processor.service';


@Injectable()
export abstract class BaseHttp {

  constructor(protected http: Http, protected errorCodeProcessor: ErrorCodeProcessor, protected router: Router) {
  }

  protected prepareHeaders(): Headers {
    return new Headers({
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    });
  }

  protected extractData(res: Response): any {
    if (res.status < 200 || res.status >= 300) {
      throw res;
    }
    return res.json() || {};
  }

  protected handleError(res: Response): Observable<IErrorInfo> {
    const error: any = res.json();
    const errorInfo: IErrorInfo = {
      code: error.code,
      statusCode: error.statusCode,
      errorMessage: this.errorCodeProcessor.getMessageByErrorCode(error.code)
    };

    return Observable.throw(errorInfo as IErrorInfo);
  }

  protected get(url: string, options?: RequestOptionsArgs): Observable<any> {
    let defaultOptions: RequestOptions = new RequestOptions({method: RequestMethod.Get});
    defaultOptions = defaultOptions.merge(options);
    return this.sendRequest(url, defaultOptions);
  }

  protected post(url: string, body: any, options?: RequestOptionsArgs): Observable<any> {
    let defaultOptions: RequestOptions = new RequestOptions({method: RequestMethod.Post, body});
    defaultOptions = defaultOptions.merge(options);
    return this.sendRequest(url, defaultOptions);
  }

  protected delete(url: string, options?: RequestOptionsArgs): Observable<any> {
    const headers: Headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    let defaultOptions: RequestOptions = new RequestOptions({headers, method: RequestMethod.Delete});
    defaultOptions = defaultOptions.merge(options);
    return this.sendRequest(url, defaultOptions);
  }

  protected patch(url: string, body: any, options?: RequestOptionsArgs): Observable<any> {
    let defaultOptions: RequestOptions = new RequestOptions({method: RequestMethod.Patch, body});
    defaultOptions = defaultOptions.merge(options);
    return this.sendRequest(url, defaultOptions);
  }

  protected put(url: string, body: any, options?: RequestOptionsArgs): Observable<any> {
    let defaultOptions: RequestOptions = new RequestOptions({method: RequestMethod.Put, body});
    defaultOptions = defaultOptions.merge(options);
    return this.sendRequest(url, defaultOptions);
  }

  protected head(url: string, options?: RequestOptionsArgs): Observable<any> {
    let defaultOptions: RequestOptions = new RequestOptions({method: RequestMethod.Put});
    defaultOptions = defaultOptions.merge(options);
    return this.sendRequest(url, defaultOptions);
  }

  private sendRequest(url: string, options: RequestOptionsArgs): Observable<string> {
    let defaultOptions: RequestOptions = new RequestOptions();
    defaultOptions.method = options.method;
    defaultOptions.headers = this.prepareHeaders();
    defaultOptions = defaultOptions.merge(options);
    return Observable.create((observer: Observer<any>) => {
      this.http.request(url, defaultOptions).subscribe((res: Response): void => {
        observer.next(this.extractData(res));
      }, (error: Response): void => {
        observer.error(this.handleError(error));
      });
    });
  }
}
