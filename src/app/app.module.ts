import {NgModule} from '@angular/core';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {MaterialModule, MdCardModule, MdInputModule} from '@angular/material';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterModule} from '@angular/router';

import {AppComponent} from './app.component';
import {AppRoutes} from './app.routing';
import {LoginComponent} from './login/login.component';
import {JobsApi} from './services/api/jobs.api.service';
import {ServicesModule} from './services/services.module';
import {UrlPairToolComponent} from './url-pair-tool/url-pair-tool.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UrlPairToolComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    MdCardModule,
    MdInputModule,
    ReactiveFormsModule,
    MaterialModule,
    RouterModule.forRoot(AppRoutes),
    FlexLayoutModule,
    ServicesModule
  ],
  providers: [JobsApi],
  bootstrap: [AppComponent]
})
export class AppModule {
}
