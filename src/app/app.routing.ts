import {Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {MainComponentGuard} from './services/guards/main.guard';
import {UrlPairToolComponent} from './url-pair-tool/url-pair-tool.component';


export const AppRoutes: Routes = [{
  path: '',
  canActivate: [MainComponentGuard],
  component: UrlPairToolComponent
}, {
  path: 'login',
  component: LoginComponent
}, {
  path: '**',
  redirectTo: 'session/404'
}];
