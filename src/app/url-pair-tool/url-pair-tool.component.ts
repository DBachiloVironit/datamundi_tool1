import {Component, ElementRef, HostListener, OnInit, ViewChild} from '@angular/core';
import {MdSnackBar, MdSnackBarConfig} from '@angular/material';
import {Router} from '@angular/router';
import {JobsApi} from '../services/api/jobs.api.service';
import {AuthService} from '../services/auth.service';
import {IErrorInfo} from '../services/error-code-processor.service';
import {IRequestResult} from '../shared/RequestResult';

@Component({
  selector: 'app-url-pair-tool',
  templateUrl: './url-pair-tool.component.html',
  styleUrls: ['./url-pair-tool.component.css']
})
export class UrlPairToolComponent implements OnInit {

  @ViewChild('source', { read: ElementRef }) public source: ElementRef;
  @ViewChild('translation', { read: ElementRef }) public translation: ElementRef;

  fuid: string;
  job: string;
  pairId: number;
  jobDone = false;

  quality: number;
  observation: number;
  enterTime = 0;
  leaveTime = 0;
  mouseMiles: number;
  scrollStateSource: number;
  scrollStateTranslation: number;

  jobName = 'Sentence Alignment Evaluation: URL Pair Qualification Tool';
  observations: any[];
  qualifications: any[];
  mouseLastPosition = {x: null, y: null};

  pair = {
    source: {
      text: '',
      lang: ''
    },
    translation: {
      text: '',
      lang: ''
    }
  };

  meta: any = {};

  constructor(private snackBar: MdSnackBar, private jobsApi: JobsApi, private authService: AuthService) {
  }

  @HostListener('document:mousemove', ['$event'])
  public onMouseMove(e: any): void {
    if (this.mouseLastPosition.x) {
      this.mouseMiles += Math.sqrt(Math.pow(this.mouseLastPosition.y - e.clientY, 2) + Math.pow(this.mouseLastPosition.x - e.clientX, 2));
    }

    this.mouseLastPosition.x = e.clientX;
    this.mouseLastPosition.y = e.clientY;
  }

  ngOnInit() {
    this.fuid = localStorage.getItem('fuid');
    this.job = localStorage.getItem('job');
    this.loadQualifications();
    this.loadObservations();
    this.loadMeta();
    this.getUserState();
  }

  public getUserState(): void {
    this.jobsApi.getUrlJobState(this.fuid, this.job)
      .subscribe((response: IRequestResult): void => {
        if (response.code === 'job_done') {
          this.jobDone = true;
          // TODO: show modal invoice page
        } else {
          this.resetData(response.data);
        }
      }, (error: IErrorInfo): void => {
        if (error.code === 'bad_fuid' || error.code === 'bad_job_name') {
          this.authService.logout();
        }
        this.showError(error.errorMessage);
      });
  }

  public loadNewJobHandler(): void {
    this.authService.logout();
  }

  public resetData(data): void {
    this.pairId = data.pair.PairID;
    this.pair = {
      source: {
        text: data.pair.source,
        lang: data.pair.srcLang
      },
      translation: {
        text: data.pair.translation,
        lang: data.pair.transLang
      }
    };
    this.quality = -1;
    this.observation = -1;
    this.enterTime = new Date().getTime();
    this.mouseMiles = 0;
    this.scrollStateSource = 0;
    this.scrollStateTranslation = 0;

    this.source.nativeElement.scrollTop = 0;
    this.translation.nativeElement.scrollTop = 0;
  }

  public loadQualifications(): void {
    this.jobsApi.getJobsQualificationsList(this.job)
      .subscribe((response: IRequestResult): void => {
        this.qualifications = response.data.qualifications;
      }, (error: IErrorInfo): void => {
        this.showError(error.errorMessage);
      });
  }

  public loadObservations(): void {
    this.jobsApi.getJobsObservationsList(this.job)
      .subscribe((response: IRequestResult): void => {
        this.observations = response.data.observations;
      }, (error: IErrorInfo): void => {
        this.showError(error.errorMessage);
      });
  }

  public loadMeta(): void {
    this.jobsApi.getJobMeta(this.job)
      .subscribe((response: IRequestResult): void => {
        this.meta = response.data.meta;
      }, (error: IErrorInfo): void => {
        this.showError(error.errorMessage);
      });
  }

  public nextHandler(): void {
    if (!this.jobDone) {
      if (this.quality === -1) {
        this.showError('Quality is not selected.');
        return;
      }

      if (this.observation === -1 && this.quality !== 2) {
        this.showError('Observation is not selected.');
        return;
      }

      this.leaveTime = new Date().getTime();
      const evaluations = {
        quality: this.quality,
        observation: this.observation,
        enterTime: this.enterTime,
        leaveTime: this.leaveTime,
        mouseMiles: this.mouseMiles,
        scrollStateSource: this.scrollStateSource,
        scrollStateTranslation: this.scrollStateTranslation
      };

      this.jobsApi.submitNext(this.fuid, this.job, this.pairId, evaluations)
        .subscribe((response: IRequestResult): void => {
          this.getUserState();
        }, (error: IErrorInfo): void => {
          this.showError(error.errorMessage);
        });
    }
  }

  public sourceScroll(event: any): void {
    const scrollState = event.target.scrollTop / event.target.scrollTopMax * 100;
    if (scrollState > this.scrollStateSource) {
      this.scrollStateSource = scrollState;
    }
  }

  public translationScroll(event: any): void {
    const scrollState = event.target.scrollTop / event.target.scrollTopMax * 100;
    if (scrollState > this.scrollStateTranslation) {
      this.scrollStateTranslation = scrollState;
    }
  }

  private showError(message: string): void {
    const snackBarRef = this.snackBar.open(`${message}`, 'Close', {
      duration: 3000
    } as MdSnackBarConfig);
    snackBarRef.onAction().subscribe(() => {
      snackBarRef.dismiss();
    });
  }

}
