import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UrlPairToolComponent } from './url-pair-tool.component';

describe('UrlPairToolComponent', () => {
  let component: UrlPairToolComponent;
  let fixture: ComponentFixture<UrlPairToolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UrlPairToolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UrlPairToolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
