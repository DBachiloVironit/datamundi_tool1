import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MdSnackBar, MdSnackBarConfig} from '@angular/material';
import {Router} from '@angular/router';
import {AuthService} from '../services/auth.service';
import {IErrorInfo} from '../services/error-code-processor.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginFormObject = {
    email: '',
    pin: '',
    job: ''
  };
  public form: FormGroup;

  constructor(private fb: FormBuilder, private router: Router, private authService: AuthService, private snackBar: MdSnackBar) {
  }

  ngOnInit() {
    this.buildForm();
  }

  buildForm(): void {
    this.form = this.fb.group({
      email: [this.loginFormObject.email, Validators.compose([Validators.required, Validators.minLength(5)])],
      pin: [this.loginFormObject.pin, Validators.compose([Validators.required, Validators.minLength(1)])],
      job: [this.loginFormObject.job, Validators.compose([Validators.required, Validators.minLength(1)])]
    });
  }

  onSubmit() {
    this.authService.login(this.form.value.email, this.form.value.pin, this.form.value.job).subscribe(() => {
      this.router.navigate(['/']);
    }, (error: IErrorInfo) => {
      this.showError(error.errorMessage);
    });
  }

  private showError(message: string): void {
    const snackBarRef = this.snackBar.open(`${message}`, 'Close', {
      duration: 3000
    } as MdSnackBarConfig);
    snackBarRef.onAction().subscribe(() => {
      snackBarRef.dismiss();
    });
  }

}
